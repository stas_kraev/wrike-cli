#!/usr/bin/env python

from rauth import OAuth1Service
import sys
import pickle
import logging

LOG = logging.getLogger('wrike-auth')

class Auth(object):
    def __init__(self,config):
        self.config=config
        self.service_name=self.config.service_name
        self.consumer_key=self.config.consumer_key
        self.consumer_secret=self.config.consumer_secret
        self.request_token_url=self.config.request_token_url
        self.access_token_url=self.config.access_token_url
        self.authorize_url=self.config.authorize_url
        self.base_url=self.config.base_url
        self.request_token=''
        self.request_token_secret=''
        self.session=''
        self.session_file=self.config.session_file

        if self.config.debug is not False:
            LOG.setLevel(logging.DEBUG)
            LOG.addHandler(logging.StreamHandler())
            LOG.debug("Auth init")


    def load_session(self):
        LOG.debug("Entering Auth::load_session")
        try:
            self.session=pickle.load(open(self.session_file, 'rb'))
            return True
        except:
            return False

    def dump_session(self):
        LOG.debug("Entering Auth::dump_session")
        s_file = open(self.session_file, 'wb')
        pickle.dump(self.session, s_file, pickle.HIGHEST_PROTOCOL)

    def auth(self):
        LOG.debug("Entering Auth::auth")
        if not self.load_session() or self.test_session() == '402':
            LOG.info("Last session failed - please renew")
            return self.renew_session()
        else:
            return self.session

    def renew_session(self):
        LOG.debug("Entering Auth::renew_session")
        self.wrike = OAuth1Service(
                name=self.service_name,
                consumer_key=self.consumer_key,
                consumer_secret=self.consumer_secret,
                request_token_url=self.request_token_url,
                access_token_url=self.access_token_url,
                authorize_url=self.authorize_url,
                base_url=self.base_url)

        self.request_token, self.request_token_secret = self.wrike.get_request_token()
        authorize_url = self.wrike.get_authorize_url(self.request_token)
        print 'Visit this URL in your browser: ' + authorize_url
        raw_input('Ready ? ... ')
        try:
            self.session = self.wrike.get_auth_session(self.request_token, self.request_token_secret)
            try:
                self.dump_session()
            except Exception as e:
                LOG.info('Failed to store session file to %s becasue of %e', self.session_file, str(e))
        except KeyError as e:
            sys.stderr.write("Auth failed " + str(e) + '\n')
            sys.exit(1)
        return self.session

    def test_session(self):
        r = self.session.post('wrike.accounts.list', { 'data' : 'none' })
        return r.status_code
        
