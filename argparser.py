#!/usr/bin/env python

import argparse
from difflib import get_close_matches
from describe import Describe

class CLIArgParser(argparse.ArgumentParser):

    # When displaying invalid choice error messages,
    # this controls how many options to show per line.
    ChoicesPerLine = 2

    def _check_value(self, action, value):
        """
        It's probably not a great idea to override a "hidden" method
        but the default behavior is pretty ugly and there doesn't
        seem to be any other way to change it.
        -- Hack is taken from aws/aws-cli project on github
        """
        # converted value must be one of the choices (if specified)
        if action.choices is not None and value not in action.choices:
            msg = ['Invalid choice, valid choices are:\n']
            for i in range(len(action.choices))[::self.ChoicesPerLine]:
                current = []
                for choice in action.choices[i:i+self.ChoicesPerLine]:
                    current.append('%-40s' % choice)
                msg.append(' | '.join(current))
            possible = get_close_matches(value, action.choices, cutoff=0.8)
            if possible:
                extra = ['\n\nInvalid choice: %r, maybe you meant:\n' % value]
                for word in possible:
                    extra.append('  * %s' % word)
                msg.extend(extra)
            raise argparse.ArgumentError(action, '\n'.join(msg))



class TopArgParser(CLIArgParser):
    def __init__(self):
        super(TopArgParser, self).__init__(add_help=False)
        self.main_opts = Describe().get_main_opts()
        self._build()


    def _build(self):
        lookup_set = self.main_opts
        for argument in lookup_set.keys():
# TODO remove code duplication from here and ArgArgumentParser._add_argument
#        self.add_argument('--config', help='Full path to alternate config file')
#        self.add_argument('--debug', action='store_true', help='Enable debug output')
#        self.add_argument('--batch', action='store_true', help='Disable auth request, fail if auth by session file not possible')
    #        self.add_argument('--session_file', help='Override session file used for auth')
            kwargs = {}
            argument_set = lookup_set[argument]
            if 'action' in argument_set.keys():
                kwargs['action']=argument_set['action']
            if 'required' in argument_set.keys():
                kwargs['required']=argument_set['required']
            if 'description' in argument_set.keys():
                kwargs['help']=argument_set['description']
            if 'default' in argument_set.keys():
                kwargs['default']=argument_set['default']

            self.add_argument('--' + argument, **kwargs)


class CommandParser(CLIArgParser):
    def __init__(self, choices_list):
        super(CommandParser, self).__init__()
        self._build(choices_list)

    def _build(self, choices_list):
        self.add_argument('command', choices=choices_list)

class SubcommandParser(CommandParser):
    pass

class ArgArgumentParser(CLIArgParser):
    def __init__(self, match_table):
        super(ArgArgumentParser, self).__init__(add_help=False)
        self._build(match_table)

    def _build(self, match_table):
        for argument in match_table.keys():
            self._add_argument(argument,match_table)

    def _add_argument(self,argument,match_table):
        attribute = match_table[argument]
        kwargs = {}
        if 'action' in attribute.keys():
            kwargs['action']=attribute['action']
        if 'required' in attribute.keys():
            kwargs['required']=attribute['required']
        if 'description' in attribute.keys():
            kwargs['help']=attribute['description']
        if 'default' in attribute.keys():
            kwargs['default']=attribute['default']

        self.add_argument('--' + argument, **kwargs)
