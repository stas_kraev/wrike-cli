#!/usr/bin/env python

import argparser
import sys
import describe

class Dummy(object):
    def __init__(self, describe=None):
        self.topargs=['--debug']
#        self.commands=['task','folder','account']
#        self.subcommands=['list','update','create','delete']
#        self.args=['--force','--dryrun','--quiet']
        self.testargs=['task', 'get', '--quiet', '--debug']

def test_top_parser():
    parser=argparser.TopArgParser(Dummy().topargs)
    parsed, remains=parser.parse_known_args(Dummy().testargs)
    print "Top args parser parsed : %s" % parsed
    print "Top args parser remains : %s" % remains
    return remains

def test_command_parser(args):
    match_table = describe.Describe().get_commands().keys()
    command_parser=argparser.CommandParser(match_table)
    parsed, remains=command_parser.parse_known_args(args)
    print "Command args parser parsed : %s" % parsed
    print "Command args parser remains : %s" % remains
    return parsed, remains

def test_subcommand_parser(command,args):
    match_table = describe.Describe().get_subcommands(command).keys()
    print "Subcommands of command %s is %s" % (command, match_table)
    command_parser=argparser.SubcommandParser(match_table)
    parsed, remains=command_parser.parse_known_args(args)
    print "Subcommand args parser parsed : %s" % parsed
    print "Subcommand args parser remains : %s" % remains
    return parsed, remains

def test_arguments_parser(command,subcommand,args):
    match_table=describe.Describe().get_attributes(command,subcommand)
    print "Arguments for %s, %s is %s" % (command, subcommand, match_table)
    argument_parser=argparser.ArgArgumentParser(match_table)
    parsed, remains=argument_parser.parse_known_args(args)
    print "Arguments parser parsed : %s" % parsed
    print "Arguments parser remains : %s" % remains
    return remains

if __name__ == '__main__':
    remains = test_top_parser()
    args = remains
    parsed, remains = test_command_parser(args)
    args = remains
    command = parsed.command
    parsed, remains = test_subcommand_parser(command, args)
    args = remains
    subcommand = parsed.command
    remains = test_arguments_parser(command,subcommand,args)
