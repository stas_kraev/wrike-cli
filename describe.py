#!/usr/bin/env python

import json

class Describe(object):
    def __init__(self, apifile="/home/void/wrike-cli/wrike.api.json", main_opts="/home/void/wrike-cli/main.opts.json"):
        self._description = json.load(open(apifile,'r'))
        self._opts = json.load(open(main_opts,'r'))

    def get_commands(self):
        res = self._description
        return res

    def get_subcommands(self,command):
        res = self._description[command]['subcommands']
        return res

    def get_attributes(self,command,subcommand):
        res = self._description[command]['subcommands'][subcommand]['attributes']
        return res

    def get_main_opts(self):
        return self._opts
