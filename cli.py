#!/usr/bin/env python

from configure import Config
from auth import Auth

if __name__ == '__main__':
    config = Config()
    wrike = Auth(config)
    session = wrike.auth()
    r = session.post('wrike.accounts.list', { 'data' : 'none' })
    print r.ok
