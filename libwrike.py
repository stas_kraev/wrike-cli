#!/usr/bin/env python

from auth import Auth
from configure import Config
from argparser import TopArgParser
from argparser import CommandParser
from argparser import SubcommandParser
from argparser import ArgArgumentParser
from help import HelpTop
from help import HelpCommand
from help import HelpSubCommand
from describe import Describe
import sys
import logging
from formatter import get_formatter
from collections import namedtuple

LOG = logging.getLogger('libwrike')
LOG.addHandler(logging.StreamHandler())

class Wrike(object):
    def __init__(self,args=None):

        if args is not None:
            self.topargs, self.remain_args = TopArgParser().parse_known_args(args)

        if self.topargs.debug:
            LOG.setLevel(logging.DEBUG)
        else:
            LOG.setLevel(logging.INFO)

        if self.topargs.config:
            LOG.debug("Overriding config for %s", self.topargs.config)
            self.config = Config(self.topargs.config, args=self.topargs).as_t
        else:
            self.config = Config(args=self.topargs).as_t

        if 'apifile' in self.config._fields:
            self.describe = Describe(self.config.apifile)
        else:
            self.describe = Describe()

        self.wrike = Auth(self.config)
        self.session = self.wrike.auth()

        if self.config.debug is not False:
            LOG.setLevel(logging.DEBUG)
            LOG.debug("Libwrike init")

        self.formatter = get_formatter(self.config.output, self.config)

        self.command, self.remain_args = self._parse_command()
        self.subcommand, self.remain_args = self._parse_subcommand()
        self.attributes, self.remain_args = self._parse_attributes()


    def _parse_attributes(self):
        LOG.debug("Entering _parse_attributes")
        if 'help' in self.remain_args:
            # rendering help for chosen subcommand and exiting
            HelpSubCommand(self.command,self.subcommand).show()
        attributes = self.describe.get_attributes(self.command, self.subcommand)
        parser = ArgArgumentParser(attributes)
        self.attributes, self.remain_args = parser.parse_known_args(self.remain_args)
        return self.attributes, self.remain_args

    def _parse_subcommand(self):
        LOG.debug("Entering _parse_subcommand")
        subcommands = self.describe.get_subcommands(self.command).keys()
        subcommands.append('help')
        parser = SubcommandParser(subcommands)
        subcommand_arg, self.remain_args = parser.parse_known_args(self.remain_args)
        if subcommand_arg.command == 'help':
            # rendering help for chosen command and exiting here
            HelpCommand(self.command).show()
        self.subcommand = subcommand_arg.command
        return self.subcommand, self.remain_args

    def _parse_command(self):
        LOG.debug("Entering _parse_command")
        commands = self.describe.get_commands().keys()
        commands.append('help')
        parser = CommandParser(commands)
        command_arg, self.remain_args = parser.parse_known_args(self.remain_args)
        if command_arg.command == 'help':
            # rendering top level helper and exiting here
            HelpTop().show()
        self.command = command_arg.command
        return self.command, self.remain_args

    def do_request(self,command,subcommand,data=None):
        LOG.debug("Entering libwrike::do_request")
        if data == None:
            data={ 'data' : 'none' }
        else:
            dlist=[x for x in data.keys() if data[x]!=None]
            tmp = data
            data = {}
            for item in dlist:
                data[item]=tmp[item]

        call = 'wrike.' + command + '.' + subcommand
        LOG.debug("Data is %s", data)
        r = self.session.post(call, data)
        if self.config.silent:
            self.formatter(self.fake_operation(self.command), r.json())
        return r

    def fake_operation(self, command_name):
        operation_class = namedtuple('operation', 'name, can_paginate')
        operation = operation_class(name=command_name, can_paginate=False) 
        return operation
