#!/usr/bin/env python

from docutils.core import publish_string
from docutils.writers import manpage
from subprocess import Popen, PIPE
import sys
from describe import Describe


class Help(object):
    def __init__ (self):
        pass

    def show(self):
        c_page=['less', '-R']
        c_groff = ['groff', '-man', '-T', 'ascii']
        p_groff = Popen(c_groff, stdin=PIPE, stdout=PIPE)
        groff_out = p_groff.communicate(input=self.man_contents)[0]
        p_page = Popen(c_page, stdin=PIPE)
        p_page.communicate(input=groff_out)
        sys.exit(1)

    def pre_render_man(self):
        process = Popen('rst2man', stdin=PIPE, stdout=PIPE)
        man_contents = process.communicate(input=self.contents)[0]
        self.man_contents = man_contents

    def aggregate_content(self,data):
        for key in data.keys():
            self.contents = self.contents + "* %s - %s\n" % (key, data[key]['description'])

    def _add_title(self,title):
        self.contents = '\n\n\n'
        s = title + '\n'
        self.contents = self.contents + '*' * len(s[:-1]) + '\n' + s + '*' * len(s[:-1]) + '\n\n'

    def _add_section(self, section, text=''):
        s = section + "\n"
        self.contents = self.contents + '=' * len(s[:-1]) + '\n' + s + '=' * len(s[:-1]) + '\n\n'
        if text:
            s = text + "\n\n"
            self.contents = self.contents + s


class HelpCommand(Help):
    def __init__ (self, command):
        self._add_title(command)
        self._add_section("Description", "Execute command")
        self._add_section("Synopsys", "wrike <command> <subcommand> [<option> ... [<option>]]")
        self._add_section("Available subcommands")
        data = Describe().get_subcommands(command)
        self.aggregate_content(data)
        self.pre_render_man()

class HelpSubCommand(Help):
    def __init__ (self,command,subcommand):
        self._add_title(command)
        self._add_section("Description", "Execute command")
        self._add_section("Synopsys", "wrike <command> <subcommand> [<option> ... [<option>]]")
        self._add_section("Available options")
        data = Describe().get_attributes(command, subcommand)
        self.aggregate_content(data)
        self.pre_render_man()

class HelpTop(Help):
    def __init__ (self):
        self._add_title("wrike")
        self._add_section("Description", "CLI wrapper around wrike API")
        self._add_section("Synopsys", "wrike <command> <subcommand> [<option> ... [<option>]]")
        self._add_section("Available commands")
        data = Describe().get_commands()
        self.aggregate_content(data)
        self.pre_render_man()

if __name__ == '__main__':
#    h = HelpSubCommand('timelog', 'filter')
#    h = HelpCommand('timelog')
    h = HelpTop()
    h.show()
