#!/usr/bin/env python

import json
import os
import sys
from collections import namedtuple

class Config(object):

    def __init__ (self, configfile=None, args=None):
        self.default = '/home/void/wrike-cli/wrike_config'
        self.fallback = '/etc/wrike_config'
        self.config = None

        if configfile is not None and os.path.isfile(configfile):
            self.configfile = configfile
        elif os.path.isfile(self.default):
            self.configfile = self.default
        elif os.path.isfile(self.fallback):
            self.configfile = self.fallback
        else:
            self.die("No more configs to try", 1)

        self.config = self.load_config(self.configfile)

        if args:
            self.config.update( {k : v for k, v in vars(args).items() if v != None or k not in self.config.keys() } )

        self.config_tuple = namedtuple('config_tuple', self.config.keys())
        self.as_t = self.config_tuple(**self.config)


    def load_config(self, configfile):
        return json.load(open(configfile,'r'))

    def __call__ (self):
        if not self.config:
            self.config=self.load_config(self.configfile)
        return self.config

    def die(self,reason,code):
        print reason
        sys.exit(code)
