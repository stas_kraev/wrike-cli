#!/usr/bin/env python

from configure import Config
from libwrike import Wrike
import sys
import logging

LOG = logging.getLogger('wrike-cli')
LOG.addHandler(logging.StreamHandler())

class CliDriver(object):
    def __init__(self):
        args = sys.argv[1:]
        self.wrike = Wrike(args=args)
        self.config = self.wrike.config
        self.command = self.wrike.command
        self.subcommand = self.wrike.subcommand
        self.attributes = self.wrike.attributes
        self.main()

    def main(self):
        r = self.wrike.do_request(self.command, self.subcommand, vars(self.attributes))
        LOG.debug("Request result %s", r.status_code)
        LOG.debug("Full result %s", r.content)

if __name__ == '__main__':
    cli = CliDriver()
